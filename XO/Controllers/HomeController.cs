﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Web;
using System.Web.Mvc;


namespace XO.Controllers
{
    public class HomeController : Controller
    {
        private XO.Business.XOAI _ai;

        public HomeController()
        {
            _ai = new Business.XOAI();
        }

        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult AiMove(Models.BoardModel b)
        {
            //sanity check the boardstate
            if (b.AiGlyph == null || b.PlayerGlyph == null || b.AiGlyph == b.PlayerGlyph 
                || b.PlayerGlyph.Count() != 1 || b.AiGlyph.Count() != 1 || b.Board.Count() != 9)
            {
                return new HttpStatusCodeResult(422);
            }

            b.AiMoveIndex = -1;
            b = _ai.EvaluateState(b);
            if (b.State == "playing")
            {
                b = _ai.AiMove(b);
                b = _ai.EvaluateState(b);
            }

            Debug.WriteLine(b.AiMoveIndex);
            Debug.WriteLine(b.Board);
            Debug.WriteLine(b.State);
            Debug.WriteLine("");

            return Json(b);
        }
    }
}