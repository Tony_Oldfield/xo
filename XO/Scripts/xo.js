﻿var naught = {
    glyph: "o",
    url: ""
};
var cross = {
    glyph: "x",
    url: ""
};
var playerMove = false;
var playerToken;
var aiToken;
var boardState = "         ";

$(document).ready(function () {

    naught.url = $(".naught-img").attr("src");
    cross.url = $(".cross-img").attr("src");
    playerToken = naught;
    aiToken = cross;
    
    $(".board-cell").bind("click", BoardCellClick);
    $(".btn-player-start").bind("click", PlayerStartMatch);
    $(".btn-ai-start").bind("click", AiStartMatch);
});

function PlayerStartMatch()
{
    NewGameInit();
    $(".game-status").html("You get to go first");
    playerMove = true;
}

function AiStartMatch()
{
    NewGameInit();
    AiMoveAndEvalAsync();
}

function NewGameInit()
{
    boardState = "         ";
    $(".btn-start").hide();
    $(".board-img").removeAttr("src");
}

function BoardCellClick() {

    if (playerMove) {
        playerMove = false;
        var i = $(this).data("index");       
        if (PlaceToken(i, playerToken)) {
            UpdateBoardState(i, playerToken.glyph)
            AiMoveAndEvalAsync();
        }
        else
            playerMove = true; //cancel move on invalid cell and let the player select again
    }
}

function PlaceToken(i, token) {

    var img = $("#img-" + i);
    if (boardState[i] == ' ') {
        $("#img-" + i).attr("src", token.url);        
        return true;
    }
    else
        return false;   
}

function UpdateBoardState(i, glyph) {

    //immutable strings so recreate with new character replacing index i
    boardState = boardState.substr(0, i) + glyph + boardState.substr(i + 1);
}

function AiMoveAndEvalAsync() {

    //post current game state to MVC and let the server handle AI
    //Done this way as learning exercise - all logic could be client side
    $.ajax({
        url: '/Home/AiMove',
        type: 'POST',
        data: JSON.stringify({
            Board: boardState,
            AiGlyph: aiToken.glyph,
            PlayerGlyph: playerToken.glyph
        }),
        contentType: 'application/json; charset=utf-8',
        success: AiMoveAndEvalSuccess,
        error: AiMoveAndEvalError
    });
}

function AiMoveAndEvalSuccess(data) {

    if (data.AiMoveIndex >= 0)
        PlaceToken(data.AiMoveIndex, aiToken);
    boardState = data.Board;
    if (data.State == "playing") {
        playerMove = true;
    }
    else {
        $(".btn-start").show();
    }
    $(".game-status").html(data.State);
}

function AiMoveAndEvalError(data) {
    $(".game-status").html("Opps .. something went wrong! Please refresh the page");
}