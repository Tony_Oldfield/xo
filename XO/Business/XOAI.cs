﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace XO.Business
{
    public class XOAI
    {   
        int[][] lines;
        
        public XOAI()
        {
            //defining the cell indexes for all possible 3 of a kind combinations
            int[] line1 = new int[3] { 0, 1, 2 };
            int[] line2 = new int[3] { 3, 4, 5 };
            int[] line3 = new int[3] { 6, 7, 8 };
            int[] line4 = new int[3] { 0, 3, 6 };
            int[] line5 = new int[3] { 1, 4, 7 };
            int[] line6 = new int[3] { 2, 5, 8 };
            int[] line7 = new int[3] { 0, 4, 8 };
            int[] line8 = new int[3] { 2, 4, 6 };
            //array of all possible 3 of a kind lines
            lines = new int[][] { line1, line2, line3, line4, line5, line6, line7, line8 };
        }
        
        public Models.BoardModel EvaluateState(Models.BoardModel b)
        {
            foreach (int[] line in lines)
                if (MatchLine(b.Board, line))
                {
                    b.State = b.Board[line[0]] + " wins";
                    return b;
                }

            if (!b.Board.Contains(" "))
            {
                b.State = "draw";
                return b;
            }

            b.State = "playing";
            return b;
        }

        public Models.BoardModel AiMove(Models.BoardModel b)
        {
            //MinMax for XO is overkill - this rules based system is also unbeatable

            //choose placement that would cause me to win
            foreach (int[] line in lines)
            {
                int moveIndex = CanMakeLine(b.Board, b.AiGlyph[0], line);
                if (moveIndex >= 0)
                    return UpdateState(moveIndex, b);
            }

            //choose placement that would block opponent from winning
            foreach (int[] line in lines)
            {
                int moveIndex = CanMakeLine(b.Board, b.PlayerGlyph[0], line);
                if (moveIndex >= 0)
                    return UpdateState(moveIndex, b);
            }

            //choose centre, or 1st available corner
            foreach (int index in new int[] { 4, 0, 2, 6, 8 })
                if (b.Board[index] == ' ')
                    return UpdateState(index, b);

            //choose 1st available position if all else fails (board should never be full if in this method)
            int i = 0;
            for (i = 0; i < 9; i++)
                if (b.Board[i] == ' ')
                    break;        
            return UpdateState(i, b);
        }

        public Models.BoardModel UpdateState(int moveIndex, Models.BoardModel b)
        {
            b.AiMoveIndex = moveIndex;
            b.Board = b.Board.Substring(0, b.AiMoveIndex) + b.AiGlyph + b.Board.Substring(b.AiMoveIndex + 1);
            return b;
        }
        
        private int CanMakeLine(string board, char glyph, int[] p)
        {
            //if there are 2 of the same tokens on a line and one empty then this returns the index of the empty cell
            //otherwise returns -1
            int count = 0;
            int n = -1;

            foreach (int i in p)
            {
                if (board[i] == glyph)
                    count++;
                else if (board[i] == ' ')
                    n = i;
            }
            return (count == 2) ? n : -1;
        }

        private bool MatchLine(string board, int[] p)
        {
            //returns true for 3 of a kind (either glyph) along the line defined by the indexes in p
            return (board[p[0]] != ' ' && board[p[0]] == board[p[1]] && board[p[0]] == board[p[2]]);
        }
    }
}