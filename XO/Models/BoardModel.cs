﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace XO.Models
{
    public class BoardModel
    {
        public string Board { get; set; }
        public string AiGlyph { get; set; }
        public string PlayerGlyph { get; set; }
        public string State { get; set; }
        public int AiMoveIndex;
    }
}